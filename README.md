# Visitor Track Detector With Email Notification

This is a PHP script that detects and logs visitors to your website using their IP address, and sends an email notification to a specified email address. It uses the ipapi.co API to retrieve the visitor's country and city based on their IP address, and the PHPMailer library to send email notifications =)

## Installation

1. Clone or download the repository to your local machine.
2. Install the required dependencies using Composer: composer install.
3. Modify the following variables in the script to match your settings:
* $mail->Username: your Gmail email address
* $mail->Password: your Gmail password or app-specific password
* $mail->setFrom: your name and email address
* $mail->addAddress: the recipient's name and email address
4. Upload the script to your website's server.
5. Add the following line to your website's header.php file (or any file that is included on every page of your website):

```php linenos
<?php
include 'path/to/visitor_track_detector.php';
?>
```
## Usage

The script logs each visitor to a text file named visitor_log.txt in the same directory as the script. Each line in the file contains the visitor's IP address, country, city, and timestamp.

The script also sends an email notification to the specified email address when a new visitor is detected. The email contains the visitor's IP address, country, city, timestamp and etc....

Note that the script uses a session variable to ensure that each visitor is logged only once during their current session. The script also uses a cookie to ensure that an email notification is sent only once per visitor per day.
