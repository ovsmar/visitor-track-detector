<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

// Start a new or resume an existing session
session_start();

// Debug statement to check if the session is being set properly
var_dump($_SESSION);

// Check if the visitor has already been logged during the current session
if (!isset($_SESSION['visitor_logged'])) {

    // Debug statement to check if the condition is being executed
    echo "Visitor not logged yet<br>";

    // Check if the cookie for email notification has been set
    if (!isset($_COOKIE['email_sent'])) {

        // Get the visitor's IP address and timestamp
        $ip = $_SERVER['REMOTE_ADDR'];

        /// Send a GET request to ipapi.co API to retrieve the visitor's country and city based on their IP address
        $url = "http://ip-api.com/json/{$ip}";
        $response = file_get_contents($url);
        $data = json_decode($response);

        // Extract the country and city from the JSON data
        $country = $data->country;
        $city = $data->city;
        $regionName = $data->regionName;
        $isp = $data->isp;
        $as = $data->as;
        $asname = $data->asname;
        $mobile = $data->mobile;

        // Check if the response was successful
        if (!$response) {
            echo "Error retrieving country information";
        }

        // Debug statement to check if the country and city variables are being set properly
        echo "Country: " . $country . "<br>";
        echo "City: " . $city . "<br>";
        echo "RegionName: " . $regionName . "<br>";
        echo "isp: " . $isp . "<br>";
        echo "as: " . $as . "<br>";
        echo "asname: " . $asname . "<br>";
        echo "mobile: " . $mobile . "<br>";

        // Set the path and filename for the text file
        $file_path = 'visitor_log.txt';

        $timestamp = time();

        // Create a new line of visitor information
        $visitor_info = "isp: " . $isp . "as: " . $as . "asname: " . $asname . "mobile: " . $mobile . "RegionName: " . $regionName . "City: " . $city . "Country: " . $country . "IP address: " . $ip . " Timestamp: " . date('Y-m-d H:i:s', $timestamp) . "\n";

        // Append the visitor information to the text file
        $result = file_put_contents($file_path, $visitor_info, FILE_APPEND | LOCK_EX);

        if ($result === false) {
            echo "Error writing to visitor_log.txt";
        }

        // Set the session variable to indicate that the visitor has been logged
        $_SESSION['visitor_logged'] = true;

        // Debug statement to check if the session variable is being set properly
        var_dump($_SESSION);

        // Array of countries for which you don't want to send emails
        $blacklist_countries = array('Russia', 'USA');

        if (!in_array($country, $blacklist_countries)) {
            // Send an email notification using PHPMailer
            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'your_email@gmail.com';
            $mail->Password = 'your_email_password';
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;

            $mail->setFrom('your_email@gmail.com', 'Your Name');
            $mail->addAddress('recipient_email@example.com', 'Recipient Name');

            $mail->Subject = 'New visitor on your website';
            $mail->Body = 'A new visitor has accessed your website.' . PHP_EOL .
                'Country: ' . $country . PHP_EOL .
                'City: ' . $city . PHP_EOL .
                'RegionName: ' . $regionName . PHP_EOL .
                'IP address: ' . $ip . PHP_EOL .
                'isp: ' . $isp . PHP_EOL .
                'as: ' . $as . PHP_EOL .
                'asname: ' . $asname . PHP_EOL .
                'mobile: ' . $mobile . PHP_EOL .
                'Timestamp: ' . date('Y-m-d H:i:s', $timestamp) . PHP_EOL;


            if (!$mail->send()) {
                echo 'Error sending email: ' . $mail->ErrorInfo;
            } else {
                // Set the cookie to indicate that the email has been sent
                setcookie('email_sent', true, time() + 24 * 60 * 60); // Cookie expires in 24h
            }
        }
    }

    // Destroy the current session
    session_destroy();
}
